import re
import sys
import os.path

class GCinvocation:
    def __init__(self, numberOfGC, numberOfFullGC, isFullGC, timeofGC, timeSinceJVMStart, youngTotalSpace, youngUsedBeforeGC, youngUsedAfterGC, edenSpaceUsedBeforeGC, edenTotalSpaceBeforeGC, fromSpaceUsedBeforeGC, fromTotalSpaceBeforeGC, oldSpaceUsedBeforeGC, oldTotalSpaceBeforeGC, edenSpaceUsedAfterGC, edenTotalSpaceAfterGC, fromSpaceUsedAfterGC, fromTotalSpaceAfterGC, oldSpaceUsedAfterGC, oldTotalSpaceAfterGC, GCReason, userTime, sysTime, realTime, TotalTimeThreadsStopped):
        self.numberOfGC = numberOfGC
        self.numberOfFullGC = numberOfFullGC
        self.isFullGC = isFullGC
        self.timeofGC = timeofGC
        self.timeSinceJVMStart = timeSinceJVMStart
        self.youngTotalSpace = youngTotalSpace
        self.youngUsedBeforeGC = youngUsedBeforeGC
        self.youngUsedAfterGC = youngUsedAfterGC
        self.edenSpaceUsedBeforeGC = edenSpaceUsedBeforeGC
        self.edenTotalSpaceBeforeGC = edenTotalSpaceBeforeGC
        self.fromSpaceUsedBeforeGC = fromSpaceUsedBeforeGC
        self.fromTotalSpaceBeforeGC = fromTotalSpaceBeforeGC
        self.oldSpaceUsedBeforeGC = oldSpaceUsedBeforeGC
        self.oldTotalSpaceBeforeGC = oldTotalSpaceBeforeGC
        self.edenSpaceUsedAfterGC = edenSpaceUsedAfterGC
        self.edenTotalSpaceAfterGC = edenTotalSpaceAfterGC
        self.fromSpaceUsedAfterGC = fromSpaceUsedAfterGC
        self.fromTotalSpaceAfterGC = fromTotalSpaceAfterGC
        self.oldSpaceUsedAfterGC = oldSpaceUsedAfterGC
        self.oldTotalSpaceAfterGC = oldTotalSpaceAfterGC
        self.GCReason = GCReason
        self.userTime = userTime
        self.sysTime = sysTime
        self.realTime = realTime
        self.TotalTimeThreadsStopped = TotalTimeThreadsStopped

    def Print(self):
        print("numberOfGC: {0}".format(self.numberOfGC))
        print("numberOfFullGC: {0}".format(self.numberOfFullGC))
        print("timeofGC: {0}".format(self.timeofGC))
        print("isFullGC: {0}".format(self.isFullGC))
        print("timeSinceJVMStart: {0}".format(self.timeSinceJVMStart))
        print("youngTotalSpace: {0}".format(self.youngTotalSpace))
        print("youngUsedBeforeGC: {0}".format(self.youngUsedBeforeGC))
        print("youngUsedAfterGC: {0}".format(self.youngUsedAfterGC))
        print("edenSpaceUsedBeforeGC: {0}".format(self.edenSpaceUsedBeforeGC))
        print("edenTotalSpaceBeforeGC: {0}".format(self.edenTotalSpaceBeforeGC))
        print("fromSpaceUsedBeforeGC: {0}".format(self.fromSpaceUsedBeforeGC))
        print("fromTotalSpaceBeforeGC: {0}".format(self.fromTotalSpaceBeforeGC))
        print("oldSpaceUsedBeforeGC: {0}".format(self.oldSpaceUsedBeforeGC))
        print("oldTotalSpaceBeforeGC: {0}".format(self.oldTotalSpaceBeforeGC))
        print("edenSpaceUsedAfterGC: {0}".format(self.edenSpaceUsedAfterGC))
        print("edenTotalSpaceAfterGC: {0}".format(self.edenTotalSpaceAfterGC))
        print("fromSpaceUsedAfterGC: {0}".format(self.fromSpaceUsedAfterGC))
        print("fromTotalSpaceAfterGC: {0}".format(self.fromTotalSpaceAfterGC))
        print("oldSpaceUsedAfterGC: {0}".format(self.oldSpaceUsedAfterGC))
        print("oldTotalSpaceAfterGC: {0}".format(self.oldTotalSpaceAfterGC))
        print("GCReason: {0}".format(self.GCReason))
        print("userTime: {0}".format(self.userTime))
        print("sysTime: {0}".format(self.sysTime))
        print("realTime: {0}".format(self.realTime))
        print("TotalTimeThreadsStopped: {0}".format(self.TotalTimeThreadsStopped))
        print("")

#def AnalyzeLog(filepath):
GCData = []
GCFinalHeap = ""
filepath = ""
if len(sys.argv) > 1:
    filepath = sys.argv[1]
CommandLineflags = "" # 43

if os.path.isfile(filepath):
    with open(filepath) as log:  
        line = log.readline()
        # 8 or 12 java
        if "1.8.0" in line:
            line = log.readline()
            CommandLineflags = log.readline()
            line = log.readline()
            if "Heap before GC invocations" not in line:
                line = log.readline()
            while line:
                match = re.search(r'Heap before GC invocations=(\d+) \(full (\d+)\)',line)
                if not match:
                    break
                numberOfGC = match.group(1)
                numberOfFullGC = match.group(2)
                line = log.readline()
                line = log.readline()

                match = re.search(r'eden space (\d+)\w, (\d+)% used',line)
                edenSpaceUsedBeforeGC = match.group(2)
                edenTotalSpaceBeforeGC = match.group(1)
                line = log.readline()

                match = re.search(r'from space (\d+)\w, (\d+)% used',line)
                fromSpaceUsedBeforeGC = match.group(2)
                fromTotalSpaceBeforeGC = match.group(1)
                line = log.readline()
                line = log.readline()

                match = re.search(r'ParOldGen.*total (\d+)K, used (\d+)K',line)
                oldSpaceUsedBeforeGC = match.group(2)
                oldTotalSpaceBeforeGC = match.group(1)
                line = log.readline()
                line = log.readline()
                line = log.readline()
                line = log.readline()

                match = re.search(r'(.*): (.*): \[.*GC \((.*)\) .*PSYoungGen: (\d+)K\-\>(\d+)K\((\d+)K\).*Times: user=(.*) sys=(.*), real=(.*) s',line)
                timeofGC = match.group(1)
                timeSinceJVMStart = match.group(2)
                GCReason = match.group(3)
                userTime = match.group(7)
                sysTime = match.group(8)
                realTime = match.group(9)
                youngTotalSpace = match.group(6)
                youngUsedBeforeGC = match.group(4)
                youngUsedAfterGC = match.group(5)
                isFullGC = False
                if "ParOldGen" in line:
                    isFullGC = True
                line = log.readline()
                line = log.readline()
                line = log.readline()

                match = re.search(r'eden space (\d+)\w, (\d+)% used',line)
                edenSpaceUsedAfterGC = match.group(2)
                edenTotalSpaceAfterGC = match.group(1)
                line = log.readline()

                match = re.search(r'from space (\d+)\w, (\d+)% used',line)
                fromSpaceUsedAfterGC = match.group(2)
                fromTotalSpaceAfterGC = match.group(1)
                line = log.readline()
                line = log.readline()

                match = re.search(r'ParOldGen.*total (\d+)K, used (\d+)K',line)
                oldSpaceUsedAfterGC = match.group(2)
                oldTotalSpaceAfterGC = match.group(1)
                line = log.readline()
                line = log.readline()
                line = log.readline()
                line = log.readline()
                line = log.readline()

                TotalTimeThreadsStopped = 0
                if "Total time for which application threads were stopped" in line:
                    match = re.search(r': (.*): Total time for which application threads were stopped: (.*) seconds,',line)
                    TotalTimeThreadsStopped = match.group(2)
                    timeSinceJVMStart = match.group(1)
                    line = log.readline()
                
                CurrGC = GCinvocation(numberOfGC, numberOfFullGC, isFullGC, timeofGC, timeSinceJVMStart, youngTotalSpace, youngUsedBeforeGC, youngUsedAfterGC, edenSpaceUsedBeforeGC, edenTotalSpaceBeforeGC, fromSpaceUsedBeforeGC, fromTotalSpaceBeforeGC, oldSpaceUsedBeforeGC, oldTotalSpaceBeforeGC, edenSpaceUsedAfterGC, edenTotalSpaceAfterGC, fromSpaceUsedAfterGC, fromTotalSpaceAfterGC, oldSpaceUsedAfterGC, oldTotalSpaceAfterGC, GCReason, userTime, sysTime, realTime, TotalTimeThreadsStopped)
                GCData.append(CurrGC)

                while "Heap before GC invocations" not in line and line != "Heap\n":
                    line = log.readline()

                if line == "Heap\n":
                    line = log.readline()
                    line = log.readline()

                    match = re.search(r'eden space (\d+)\w, (\d+)% used',line)
                    edenSpaceUsedBeforeGC = match.group(2)
                    edenTotalSpaceBeforeGC = match.group(1)
                    line = log.readline()

                    match = re.search(r'from space (\d+)\w, (\d+)% used',line)
                    fromSpaceUsedBeforeGC = match.group(2)
                    fromTotalSpaceBeforeGC = match.group(1)
                    line = log.readline()
                    line = log.readline()

                    match = re.search(r'ParOldGen.*total (\d+)K, used (\d+)K',line)
                    oldSpaceUsedBeforeGC = match.group(2) 
                    oldTotalSpaceBeforeGC = match.group(1)
                    line = log.readline()
                    line = log.readline()
                    line = log.readline()
                    line = log.readline()
                    GCFinalHeap = GCinvocation(numberOfGC, numberOfFullGC, False, 0, timeSinceJVMStart, youngTotalSpace, 0, 0, edenSpaceUsedBeforeGC, edenTotalSpaceBeforeGC, fromSpaceUsedBeforeGC, fromTotalSpaceBeforeGC, oldSpaceUsedBeforeGC, oldTotalSpaceBeforeGC, 0, edenTotalSpaceAfterGC, 0, fromTotalSpaceAfterGC, 0, oldTotalSpaceAfterGC, "", 0, 0, 0, 0)
   
        else:
            line = log.readline()
            line = log.readline()
            numberOfFullGC = 0
            while line:
                #timeSinceJVMStart = 0
                if "Heap" in line:
                    line = log.readline()
                    line = log.readline()

                    match = re.search(r'eden space (\d+)\w, (\d+)% used',line)
                    edenSpaceUsedBeforeGC = match.group(2)
                    edenTotalSpaceBeforeGC = match.group(1)
                    line = log.readline()

                    match = re.search(r'from space (\d+)\w, (\d+)% used',line)
                    fromSpaceUsedBeforeGC = match.group(2)
                    fromTotalSpaceBeforeGC = match.group(1)
                    line = log.readline()
                    line = log.readline()

                    match = re.search(r'ParOldGen.*total (\d+)K, used (\d+)K',line)
                    oldSpaceUsedBeforeGC = match.group(2) 
                    oldTotalSpaceBeforeGC = match.group(1)
                    line = log.readline()
                    match = re.search(r'\[(.*)s\]',line)
                    timeSinceJVMStart = match.group(1)
                    line = log.readline()
                    line = log.readline()
                    line = log.readline()
                    GCFinalHeap = GCinvocation(0, numberOfFullGC, False, 0, timeSinceJVMStart, 0, 0, 0, edenSpaceUsedBeforeGC, edenTotalSpaceBeforeGC, fromSpaceUsedBeforeGC, fromTotalSpaceBeforeGC, oldSpaceUsedBeforeGC, oldTotalSpaceBeforeGC, 0, 0, 0, 0, 0, 0, "", 0, 0, 0, 0)
                    
                else:
                    match = re.search(r'\[(.*)s\].*GC\((\d+)\).*\((.*)\)',line)
                    timeSinceJVMStart = match.group(1)
                    numberOfGC = match.group(2)
                    GCReason = match.group(3)

                    if "Pause Full" in line:
                        isFullGC = True
                        numberOfFullGC += 1
                        line = log.readline()
                        line = log.readline()
                        line = log.readline()
                        line = log.readline()
                        line = log.readline()
                        line = log.readline()
                        line = log.readline()
                        line = log.readline()
                        line = log.readline()
                        line = log.readline()
                        print(line)
                    else:
                        isFullGC = False
                    
                    line = log.readline()
                    match = re.search(r'(\d+)K\-\>(\d+)K\((\d+)K\)',line)
                    youngUsedBeforeGC = match.group(1)
                    youngUsedAfterGC = match.group(2)
                    youngTotalSpace = match.group(3)
                    line = log.readline()

                    match = re.search(r'(\d+)K\-\>(\d+)K\((\d+)K\)',line)
                    oldSpaceUsedBeforeGC = match.group(1)
                    oldSpaceUsedAfterGC = match.group(2)
                    oldTotalSpaceBeforeGC = match.group(3)
                    line = log.readline()
                    line = log.readline()
                    
                    match = re.search(r'(\d+\.\d+)ms',line)
                    TotalTimeThreadsStopped = float(match.group(1))/1000
                    line = log.readline()

                    match = re.search(r'User=(.*)s Sys=(.*)s Real=(.*)s',line)
                    userTime = match.group(1)
                    sysTime = match.group(2)
                    realTime = match.group(3)
                    line = log.readline()
                    timeofGC = 0
                    edenSpaceUsedBeforeGC = 0
                    edenTotalSpaceBeforeGC = 0
                    fromSpaceUsedBeforeGC = 0
                    fromTotalSpaceBeforeGC = 0
                    edenSpaceUsedAfterGC = 0
                    edenTotalSpaceAfterGC = 0
                    fromSpaceUsedAfterGC = 0 
                    fromTotalSpaceAfterGC = 0
                    oldTotalSpaceAfterGC = oldTotalSpaceBeforeGC
                    CurrGC = GCinvocation(numberOfGC, numberOfFullGC, isFullGC, timeofGC, timeSinceJVMStart, youngTotalSpace, youngUsedBeforeGC, youngUsedAfterGC, edenSpaceUsedBeforeGC, edenTotalSpaceBeforeGC, fromSpaceUsedBeforeGC, fromTotalSpaceBeforeGC, oldSpaceUsedBeforeGC, oldTotalSpaceBeforeGC, edenSpaceUsedAfterGC, edenTotalSpaceAfterGC, fromSpaceUsedAfterGC, fromTotalSpaceAfterGC, oldSpaceUsedAfterGC, oldTotalSpaceAfterGC, GCReason, userTime, sysTime, realTime, TotalTimeThreadsStopped)
                    GCData.append(CurrGC)

    isEndProgram = False
    isAdaptiveSize = False
    isEmptyArray = False

    allocatedYoungSpace = 0 # 1
    allocatedOldSpace = 0 # 1
    PeakYoungUsed = 0 # 2
    PeakOldUsed = 0 # 2

    totalGCPauseTime = 0 # 5,9
    avgCGPauseTime = 0 # 3,10
    maxGCPauseTime = 0 # 4,11
    totalAppRunTime = 0 # 6
    troughput = 0 # 7 

    totalCGCount = 0 # 8
    minGCTime = 0 # 11
    totalGCIntervalTime = 0 # 12
    avgGCIntervaltime = 0 # 12
    minorGCcount = 0 # 13
    totalMinorGCTime = 0 # 14
    avgMinorGCTime = 0 # 15

    minMinorGCTime = 0 # 16
    maxMinorGCTime = 0 # 16
    avgMinorGCIntervalTime = 0 # 17
    totalMinorGCIntervalTime = 0 # 17
    fullGCCount = 0 # 18
    totalFullGCTime = 0 # 19
    avgFullGCTime = 0 # 20
    minFullGCTime = 0 # 21
    maxFullGCTime = 0 # 21
    avgFullGCIntervalTime = 0 # 22
    totalFullGCIntervalTime = 0 # 22

    totalCreatedBytes = 0 # 23
    totalPromotedBytes = 0 # 24
    avgCreationRate = 0 # 25
    avgPromotionRate = 0 # 26

    ergonomicsCount = 0 # 27
    avgErgonomicsTime = 0 # 28
    maxErgonomicsTime = 0 # 29
    totalErgonomicsTime = 0 # 30
    allocation_FailureCount = 0 # 31
    avgAllocation_FailureTime = 0 # 32
    maxAllocation_FailureTime = 0 # 33
    totalAllocation_FailureTime = 0 # 34
    gC_systemCount = 0 # 35
    avgGC_systemTime = 0 # 36
    maxGC_systemTime = 0 # 37
    totalGC_systemTime = 0 # 38
    otherCausesCount = 0 # 39
    avgOtherCausesTime = 0 # 40
    maxOtherCausesTime = 0 # 41
    totalOtherCausesTime = 0 # 42

    totalReclaimedBytes = 0 # 44 (minorGCReclaimed + fullGCReclaimed)
    minorGCReclaimed = 0 # 45
    fullGCReclaimed = 0 # 46

    fullGCNotEffectiveCounter = 0 # 49
    longSyscallsGCAray = [] # 50

    if len(GCData) == 0:
        isEndProgram = True
        isEmptyArray = True

    for gc in GCData:
        if (gc.edenTotalSpaceBeforeGC != gc.edenTotalSpaceAfterGC or  gc.fromTotalSpaceBeforeGC != gc.fromTotalSpaceAfterGC or gc.oldTotalSpaceBeforeGC != gc.oldTotalSpaceAfterGC):
            isEndProgram = True
            isAdaptiveSize = True

    if isEndProgram:
        if isEmptyArray:
            print("No GC occurred")
        elif isAdaptiveSize:
            print("Adaptive size in an anti-pattern")
        print()
        print("Exitting the program.")

    # TODO: 
    # JVM Heap Size
    # -------------
    # 1. Allocated young and old spaces V
    # 2. Peak young and old             V
    #
    # Key Performance Indicators
    # --------------------------
    # 3. Avg Pause GC Time              V
    # 4. Max Pause GC Time              V
    # 5. Total CG Pause time            V
    # 6. Total App runtime              V
    # 7. Throughput = 100 - (5/6)*100   V
    #
    # GC Statistics
    # -------------
    # 8. Total GC count                 V
    # 9. Total GC time                  V
    # 10. Avg GC time                   V
    # 11. GC min/max time               V
    # 12. GC Interval avg time          V
    # 13. Minor GC count                V
    # 14. Minor GC total time           V
    # 15. Minor GC avg time             V
    # 16. Minor GC min/max time         V
    # 17. Minor GC Interval avg         V
    # 18. Full GC Count                 V
    # 19. Full GC total time            V
    # 20. Full GC avg time              V
    # 21. Full GC min/max time          V
    # 22. Full GC Interval avg          V
    #
    # Object Stats
    # -----------
    # 23. Total created bytes           V
    # 24. Total promoted bytes          V
    # 25. Avg creation rate             V
    # 26. Avg promotion rate            V
    #
    # GC Causes
    # ---------
    # 27. Ergonomics count              V
    # 28. Ergonomics Avg Time           V
    # 29. Ergonomics Max Time           V
    # 30. Ergonomics Total Time         V
    # 31. Allocation Failure count      V
    # 32. Allocation Failure Avg Time   V
    # 33. Allocation Failure Max Time   V
    # 34. Allocation Failure Total Time V
    # 35. gc.system count               V
    # 36. gc.system Avg Time            V
    # 37. gc.system Max Time            V
    # 38. gc.system Total Time          V
    # 39. Other count                   V
    # 40. Other Avg Time                V
    # 41. Other Max Time                V
    # 42. Other Total Time              V
    # 
    # Command Line Flags
    # ------------------
    # 43. Command Line Flags            V
    #
    # Reclaimed bytes
    # ---------------
    # 44. Total reclaimed bytes         V
    # 45. Minor GC reclaimed            V
    # 46. Full GC reclaimed             V
    # 
    # Conditions for Performance
    # --------------------------
    # 47. Amount of bytes created / creation rate is bigger than half the size of the young space   V
    # 49. If after full GC the old space is still full then bigger old space is needed              V
    # 50. GC took a relativly long time on the syscalls in GC {0}. please check OS status           V
    # 51. If Full reclamed is bigger than Minor reclamed than young needs to grow                   V
    # 52. If throughput is smaller then 95% than the time the app is working on GC is too long and it is 
    #     needed to increase memory space or decrease byte creatin rate                             V
    # 53. If the number of non-Ergonomics/non-Allocation Failure CGs is bigger than 1 then ask if 
    #     there is a reason to call that CG in the code or in the JVM                               V

    if not isEndProgram:
        totalAppRunTime = float(GCFinalHeap.timeSinceJVMStart)
        allocatedYoungSpace = int(GCData[0].youngTotalSpace) 
        allocatedOldSpace = int(GCData[0].oldTotalSpaceBeforeGC)
        minGCTime = float(GCData[0].TotalTimeThreadsStopped)
        totalCGCount = len(GCData)
        fullGCCount = int(GCFinalHeap.numberOfFullGC)
        minorGCcount = totalCGCount - fullGCCount
        minMinorGCTime = totalAppRunTime
        minFullGCTime = totalAppRunTime

        GCIndex = 0
        MinorGCIndex = 0
        FullGCIndex = 0

        lastRelativeCGTime = 0
        lastRelativeMinorCGTime = 0
        lastRelativeFullCGTime = 0
        lastYoungUsedAfterGC = 0
        lastoldSpaceUsedAfterGC = 0

        minorGCReclaimed1 = 0


        for gc in GCData:
            if float(gc.sysTime)/float(gc.userTime) > 0.1:
                longSyscallsGCAray.append(gc.numberOfGC)

            if int(gc.youngUsedBeforeGC) > PeakYoungUsed:
                PeakYoungUsed = int(gc.youngUsedBeforeGC)
            
            if int(gc.oldSpaceUsedBeforeGC) > PeakOldUsed:
                PeakOldUsed = int(gc.oldSpaceUsedBeforeGC)
            totalGCPauseTime += float(gc.TotalTimeThreadsStopped)

            if lastRelativeCGTime != 0:
                totalGCIntervalTime += float(gc.timeSinceJVMStart) - lastRelativeCGTime
            lastRelativeCGTime = float(gc.timeSinceJVMStart)

            totalCreatedBytes += int(gc.youngUsedBeforeGC) - lastYoungUsedAfterGC
            lastYoungUsedAfterGC = int(gc.youngUsedAfterGC)

            if float(gc.TotalTimeThreadsStopped) > maxGCPauseTime:
                maxGCPauseTime = float(gc.TotalTimeThreadsStopped)
                
            if float(gc.TotalTimeThreadsStopped) < minGCTime:
                minGCTime = float(gc.TotalTimeThreadsStopped)
            
            if not gc.isFullGC:
                totalMinorGCTime += float(gc.TotalTimeThreadsStopped)

                #minorGCReclaimed1 += int(gc.youngUsedBeforeGC) - (int(gc.fromTotalSpaceAfterGC) * ((int(gc.fromSpaceUsedAfterGC) - int(gc.fromSpaceUsedBeforeGC))/100) + int(gc.oldSpaceUsedAfterGC) - int(gc.oldSpaceUsedBeforeGC))
                minorGCReclaimed += int(gc.youngUsedBeforeGC) - (int(gc.youngUsedAfterGC) + int(gc.oldSpaceUsedAfterGC) - int(gc.oldSpaceUsedBeforeGC))

                if lastRelativeMinorCGTime != 0:
                    totalMinorGCIntervalTime += float(gc.timeSinceJVMStart) - lastRelativeMinorCGTime
                lastRelativeMinorCGTime = float(gc.timeSinceJVMStart)

                if float(gc.TotalTimeThreadsStopped) > maxMinorGCTime:
                    maxMinorGCTime = float(gc.TotalTimeThreadsStopped)
                
                if float(gc.TotalTimeThreadsStopped) < minMinorGCTime:
                    minMinorGCTime = float(gc.TotalTimeThreadsStopped)
            else:
                totalFullGCTime += float(gc.TotalTimeThreadsStopped)

                fullGCReclaimed += (int(gc.youngUsedBeforeGC) + int(gc.oldSpaceUsedBeforeGC)) - (int(gc.youngUsedAfterGC) + int(gc.oldSpaceUsedAfterGC))

                if lastRelativeFullCGTime != 0:
                    totalFullGCIntervalTime += float(gc.timeSinceJVMStart) - lastRelativeFullCGTime
                lastRelativeFullCGTime = float(gc.timeSinceJVMStart)

                if float(gc.TotalTimeThreadsStopped) > maxFullGCTime:
                    maxFullGCTime = float(gc.TotalTimeThreadsStopped)
                
                if float(gc.TotalTimeThreadsStopped) < minFullGCTime:
                    minFullGCTime = float(gc.TotalTimeThreadsStopped)

                totalPromotedBytes += int(gc.oldSpaceUsedBeforeGC) - lastoldSpaceUsedAfterGC
                lastoldSpaceUsedAfterGC = int(gc.oldSpaceUsedBeforeGC)

                if ((int(gc.oldSpaceUsedBeforeGC) - int(gc.oldSpaceUsedAfterGC))/float(gc.oldTotalSpaceAfterGC)) < 0.1:
                    fullGCNotEffectiveCounter += 1


            if gc.GCReason == "Allocation Failure":
                allocation_FailureCount += 1
                totalAllocation_FailureTime += float(gc.TotalTimeThreadsStopped)
                if float(gc.TotalTimeThreadsStopped) > maxAllocation_FailureTime:
                    maxAllocation_FailureTime = float(gc.TotalTimeThreadsStopped)
            elif gc.GCReason == "Ergonomics":
                ergonomicsCount += 1
                totalErgonomicsTime += float(gc.TotalTimeThreadsStopped)
                if float(gc.TotalTimeThreadsStopped) > maxErgonomicsTime:
                    maxErgonomicsTime = float(gc.TotalTimeThreadsStopped)
            elif gc.GCReason == "System.gc()":
                gC_systemCount += 1
                totalGC_systemTime += float(gc.TotalTimeThreadsStopped)
                if float(gc.TotalTimeThreadsStopped) > maxGC_systemTime:
                    maxGC_systemTime = float(gc.TotalTimeThreadsStopped)
            else:
                otherCausesCount += 1
                totalOtherCausesTime += float(gc.TotalTimeThreadsStopped)
                if float(gc.TotalTimeThreadsStopped) > maxOtherCausesTime:
                    maxOtherCausesTime = float(gc.TotalTimeThreadsStopped)

            
            
        #    gc.Print()
        #print("Final Heap:")
        #GCFinalHeap.Print()
        #print()
        #print(CommandLineflags)

        avgCGPauseTime = totalGCPauseTime/totalCGCount
        troughput = 100 - 100*(totalGCPauseTime/totalAppRunTime)
        totalReclaimedBytes = minorGCReclaimed + fullGCReclaimed

        if minorGCcount != 0:
            avgMinorGCTime = totalMinorGCTime/minorGCcount

        if fullGCCount != 0:
            avgFullGCTime = totalFullGCTime/fullGCCount

        if allocation_FailureCount != 0:
            avgAllocation_FailureTime = totalAllocation_FailureTime/allocation_FailureCount

        if ergonomicsCount != 0:
            avgErgonomicsTime = totalErgonomicsTime/ergonomicsCount

        if gC_systemCount != 0:
            avgGC_systemTime = totalGC_systemTime/gC_systemCount

        if otherCausesCount != 0:
            avgOtherCausesTime = totalOtherCausesTime/otherCausesCount

        if totalCGCount > 1:
            avgGCIntervaltime = totalGCIntervalTime/(totalCGCount - 1)

        if minorGCcount > 1:
            avgMinorGCIntervalTime = totalMinorGCIntervalTime/(minorGCcount - 1)

        if fullGCCount > 1:
            avgFullGCIntervalTime = totalFullGCIntervalTime/(fullGCCount - 1)

        totalPromotedBytes += int(GCFinalHeap.oldSpaceUsedBeforeGC) - lastoldSpaceUsedAfterGC
        avgCreationRate = totalCreatedBytes/totalAppRunTime 
        avgPromotionRate = totalPromotedBytes/totalAppRunTime

        print("Analysis of log file {0}:".format(filepath))
        print()
        print(CommandLineflags)
        print()

        print("JVM Heap Size:")
        print("    - Young Generation Allocated: {0}K".format(allocatedYoungSpace))
        print("    - Young Generation Peak: {0}K".format(PeakYoungUsed))
        print("    - Old Generation Allocated: {0}K".format(allocatedOldSpace))
        print("    - Old Generation Peak: {0}K".format(PeakOldUsed))
        print()

        print("Key Performance Indicators:")
        print("    - Avg Pause GC Time: {0}s".format(avgCGPauseTime))
        print("    - Max Pause GC Time: {0}s".format(maxGCPauseTime))
        print("    - Total CG Pause time: {0}s".format(totalGCPauseTime))
        print("    - Total App runtime: {0}s".format(totalAppRunTime))
        print("    - Throughput: {0}".format(troughput))
        print()

        print("GC Statistics:")
        print("    - Total GC count: {0}".format(totalCGCount))
        print("    - Total GC time: {0}s".format(totalGCPauseTime))
        print("    - Avg GC time: {0}s".format(avgCGPauseTime))
        print("    - GC min/max time: {0}s/{1}s".format(minGCTime,maxGCPauseTime))
        print("    - GC Interval avg time: {0}s".format(avgGCIntervaltime))
        print("    - Minor GC count: {0}".format(minorGCcount))
        print("    - Minor GC total time: {0}s".format(totalMinorGCTime))
        print("    - Minor GC avg time: {0}s".format(avgMinorGCTime))
        print("    - Minor GC min/max time: {0}s/{1}s".format(minMinorGCTime,maxMinorGCTime))
        print("    - Minor GC Interval avg time: {0}s".format(avgMinorGCIntervalTime))
        print("    - Full GC Count: {0}".format(fullGCCount))
        print("    - Full GC total time: {0}s".format(totalFullGCTime))
        print("    - Full GC avg time: {0}s".format(avgFullGCTime))
        print("    - Full GC min/max time: {0}s/{1}s".format(minFullGCTime,maxFullGCTime))
        print("    - Full GC Interval avg time: {0}s".format(avgFullGCIntervalTime))
        print()

        print("Object Stats:")
        print("    - Total created bytes: {0}K".format(totalCreatedBytes))
        print("    - Total promoted bytes: {0}K".format(totalPromotedBytes))
        print("    - Avg creation rate: {0}KB/sec".format(avgCreationRate))
        print("    - Avg promotion rate: {0}KB/sec".format(avgPromotionRate))
        print()

        print("GC Causes:")
        if allocation_FailureCount > 0:
            print("    - Allocation Failure count: {0}".format(allocation_FailureCount))
            print("    - Allocation Failure Avg Time: {0}s".format(avgAllocation_FailureTime))
            print("    - Allocation Failure Max Time: {0}s".format(maxAllocation_FailureTime))
            print("    - Allocation Failure Total Time: {0}s".format(totalAllocation_FailureTime))
        if ergonomicsCount > 0:
            print("    - Ergonomics count: {0}".format(ergonomicsCount))
            print("    - Ergonomics Avg Time: {0}s".format(avgErgonomicsTime))
            print("    - Ergonomics Max Time: {0}s".format(maxErgonomicsTime))
            print("    - Ergonomics Total Time: {0}s".format(totalErgonomicsTime))
        if gC_systemCount > 0:
            print("    - gc.system count: {0}".format(gC_systemCount))
            print("    - gc.system Avg Time: {0}s".format(avgGC_systemTime))
            print("    - gc.system Max Time: {0}s".format(maxGC_systemTime))
            print("    - gc.system Total Time: {0}s".format(totalGC_systemTime))
        if otherCausesCount > 0:
            print("    - Other count: {0}".format(otherCausesCount))
            print("    - Other Avg Time: {0}s".format(avgOtherCausesTime))
            print("    - Other Max Time: {0}s".format(maxOtherCausesTime))
            print("    - Other Total Time: {0}s".format(totalOtherCausesTime))
        print()
 
        print("Reclaimed bytes:")
        print("    - Total reclaimed bytes: {0}K".format(totalReclaimedBytes))
        print("    - Minor GC reclaimed: {0}K".format(minorGCReclaimed))
        #print("    - Minor GC reclaimed1: {0}K".format(minorGCReclaimed1))
        print("    - Full GC reclaimed: {0}K".format(fullGCReclaimed))
        print()

        print("Recomendations:")
        if avgCreationRate > (allocatedYoungSpace/2):
            print("    - The byte creation rate is very high. Try reducing the creation rate or increasing the young space if necessary.")
            print()

        if fullGCReclaimed > minorGCReclaimed:
            print("    - The amount of reclaimed bytes from full GCs is larger than the amount of reclaimde bytes from minor GCs.")
            print("      It is recommended to increase the young space to avoid unnecessary full GCs.")
            print()

        if troughput < 95:
            print("    - The time the app is working on GC is too long and it is recommended to increase memory space or decrease byte creatin rate.")
            print()

        if gC_systemCount + otherCausesCount > 0:
            print("    - it is unrecommended to call a GC that is not caused by allocation failure or ergonomics")
            print()

        if fullGCNotEffectiveCounter > 2:
            print("    - The amount of static or non-reclaimble bytes in the old space was to large. It is recommended to increase the old space to avoid unnecessary full GCs. ")
            print()

        for GCnum in longSyscallsGCAray:
            print("    - GC took a relativly long time on the syscalls in GC {0}. please check OS status".format(GCnum))
            print()

            