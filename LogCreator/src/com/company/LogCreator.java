package com.company;


import java.util.HashMap;
import java.util.Map;

public class LogCreator {

    private static Map<String, String> stringContainer = new HashMap<>();

    public static void main(String[] args) {
        System.out.println("Start of program!");
        String stringWithPrefix = "stringWithPrefix";

        int size = 1;
        int strLength = 3*size;
        int strToRemove = 2*size;

        // Load Java Heap with 3 M java.lang.String instances
        for (int i = 0; i < strLength; i++) {
            String newString = stringWithPrefix + i;
            stringContainer.put(newString, newString);
        }
        System.out.println("MAP size: " + stringContainer.size());

        // Explicit GC!
        //System.gc();

        // Remove 2 M out of 3 M
        for (int i = 0; i < strToRemove; i++) {
            String newString = stringWithPrefix + i;
            stringContainer.remove(newString);
        }

        System.out.println("MAP size: " + stringContainer.size());
        System.out.println("End of program!");
    }
}